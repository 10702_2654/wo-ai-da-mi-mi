package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button B1;
    public Button B2;
    public Button B3;
    public Button B4;
    public Button B5;
    public Label L1;
    public Label L2;
    public Label L3;
    public Label L4;
    public Label L5;
    public Button B9;
    public Label L9;
    public Button B10;
    public Button B11;
    public Button B12;
    public Button B13;
    public Button B14;
    public Button B15;
    public Button B16;
    public Button B17;
    public Button B18;
    public Button B19;
    public Button B20;
    int a=0,b=0,c=0,d=0,e=0;

    public void OB1(ActionEvent actionEvent) {
        a++;
        L1.setText("廣東包"+a+"個");
    }

    public void OB2(ActionEvent actionEvent) {
        b++;
        L2.setText("叉燒包"+b+"個");
    }

    public void OB3(ActionEvent actionEvent) {
        c++;
        L3.setText("上海包"+c+"個");
    }

    public void OB4(ActionEvent actionEvent) {
        d++;
        L4.setText("福連包"+d+"個");
    }

    public void OB5(ActionEvent actionEvent) {
        e++;
        L5.setText("小籠包"+e+"個");
    }

    public void OB9(ActionEvent actionEvent) {
        int sum;
        sum=a*25+b*30+c*20+d*25+e*40;
        L9.setText("總金額:"+sum+"元");
    }


    public void OB11(ActionEvent actionEvent) {
        a++;
        L1.setText("廣東包"+a+"個");
    }

    public void OB12(ActionEvent actionEvent) {
        b++;
        L2.setText("叉燒包"+b+"個");
    }

    public void OB13(ActionEvent actionEvent) {
        c++;
        L3.setText("上海包"+c+"個");
    }

    public void OB14(ActionEvent actionEvent) {
        d++;
        L4.setText("福連包"+d+"個");
    }

    public void OB15(ActionEvent actionEvent) {
        e++;
        L5.setText("小籠包"+e+"個");
    }

    public void OB16(ActionEvent actionEvent) {
        if(a>0)
        {
            a--;
            L1.setText("廣東包"+a+"個");
        }

    }

    public void OB17(ActionEvent actionEvent) {
        if(b>0) {
            b--;
            L2.setText("叉燒包" + b + "個");
        }
    }

    public void OB18(ActionEvent actionEvent) {
        if(c>0) {
            c--;
            L3.setText("上海包" + c + "個");
        }
    }

    public void OB19(ActionEvent actionEvent) {
        if(d>0) {
            d--;
            L4.setText("福連包" + d + "個");
        }
    }

    public void OB20(ActionEvent actionEvent) {
        if(e>0) {
            e--;
            L5.setText("小籠包" + e + "個");
        }
    }

    public void OB10(ActionEvent actionEvent) {
        a=0;
        b=0;
        c=0;
        d=0;
        e=0;
        L1.setText("");
        L2.setText("");
        L3.setText("");
        L4.setText("");
        L5.setText("");
        L9.setText("總金額:"+"");
    }
}
